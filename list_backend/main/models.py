from django.db import models

# Create your models here.


class List(models.Model):
    content = models.CharField(max_length=300)
    UUID = models.CharField(max_length=20)
    is_completed = models.BooleanField(default=False)
    dead_line = models.DateField(null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content


class Order(models.Model):
    order = models.TextField()



