from django.contrib import admin
from .models import List, Order
# Register your models here.

admin.site.register(List)
admin.site.register(Order)