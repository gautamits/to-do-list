from django.urls import path
from .views import reposition, DisplayLists, ListDetailView
urlpatterns = [
    path('todos/reposition/', reposition),
    path('todos/', DisplayLists.as_view()),
    path('todos/<str:UUID>', ListDetailView.as_view()),
]
