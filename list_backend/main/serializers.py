from rest_framework import serializers
from .models import List

class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = List
        fields = ('content', 'UUID', 'is_completed', 'dead_line')






