from .models import List, Order
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import ListSerializer
from rest_framework import generics
from django.db.models import Case, When


class ListDetailView(generics.RetrieveUpdateDestroyAPIView):
    lookup_field = 'UUID'
    queryset = List.objects.all()
    serializer_class = ListSerializer


class DisplayLists(generics.ListCreateAPIView):
    queryset = List.objects.all()
    serializer_class = ListSerializer

    def get_queryset(self):
        try:
            order = Order.objects.all().first().order.split(",")
            preserved = Case(*[When(UUID=uuid, then=pos) for pos, uuid in enumerate(order)])
            queryset = List.objects.all().order_by(preserved)
            return ListSerializer(queryset, many=True).data

        except Exception as Error:
            print(Error)
            return ListSerializer(List.objects.all(), many=True).data


@api_view(['POST'])
def reposition(request):
    data = request.data
    print(data)
    list_items = data['list']

    order = Order.objects.all()
    if len(order) > 0:
        order[0].order = ",".join(list_items)
        order[0].save()
    else:
        order = Order(order=",".join(list_items))
        order.save()
    return Response({"success"})








