import path from 'path';
const config = {
  mode: "development",
  entry: path.resolve(__dirname, 'src/index.js'),

  // End result of webpack's work is dumped into this file
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },
  devServer: {
      inline: true,
      port: 8080
  },
  module: {
      rules: [{
          test: /\.jsx$/,
          exclude: /node_modules/,
          use: {
              loader: "babel-loader"
          }
      },
      {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
              loader: "babel-loader"
          }
      },
      {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
      }
      ]
  },
  resolve: {
    // Allows use to ommit the extension when importing a file.
    // we just import / require .. './filename' instead of './filename.js' or
    // './filename.jsx'
    extensions: ['.jsx', '.js']
  },

  // Turn debugging on, so we can debug in es6 and not the transpiled es5
  devtool: "source-map"
}

export default config;

