import React, { Component } from "react";
import Label from '../SingleLabel/singleLabel.js'
import styles from './singleToDo.css';
class SingleTodo extends Component{
    constructor(props){
        super(props);
        this.state = {
            showInput: false,
            text: props.todo,
        }
    }
    
     update_status = e => {    
        this.props.changeCompletionStatus(this.props.index);
        
         
     }
    toggleEditable = (e) => {
        //console.log(this.props)
        if(this.state.showInput){
            this.props.update(this.props.index, this.state.text);
        }
        this.setState({showInput:!this.state.showInput})
    }

    updateTodo = (e) => {
            let txt = e.target.value;
            this.setState({text:txt});
    }
    update_date = (e) => {
        let date=e.target.value
        e.preventDefault();
        this.props.updateDate(this.props.index, date)
    }
    render(){
            return (
                <li className="ui-state-default"
                draggable="true" {...this.props.dragprops}>
                    <div className="text">
                        <input type="checkbox" onChange={this.update_status} checked={this.props.is_completed}/>
                        {(this.state.showInput) ?
                                <input type="text" value={this.state.text} onChange={this.updateTodo}/>
                            :
                                <Label todo={this.props.todo}/>
                        }
                        
                    </div>
                    <div className="controller">
                        <input type="date" value={this.props.deadline} onChange={this.update_date}/>
                        <i onClick={this.toggleEditable} className="fa fa-edit"></i>
                        <i onClick={this.props.delete} className="fa fa-trash"></i>
                    </div>
                </li>
            ) 
        }
    }


export default SingleTodo;