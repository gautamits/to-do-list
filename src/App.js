import React, { Component } from 'react';
import './App.css';
import SingleTodo from './SingleToDo/singleToDo.js';
import InputForm from './InputForm/InputForm.js';

const root_url = 'http://127.0.0.1:8000/api/todos/'
const headers = {
  'Accept': 'application/json, text/plain, */*',
  'Content-Type': 'application/json'
}

class App extends Component {
  constructor(){
    super();
    fetch(root_url)
    .then(data => data.json())
    .then(data => {
      this.setState({todos:data})
    })
    this.state = {
      todos: [],
      dragSrcEl: null,
    };
  }

  onInputChange = e => {
    if(e.key === 'Enter'){
      let text=e.target.value;
      e.preventDefault();
      this.onClick(text);
    }
    else{
      e.preventDefault();
    }
    
  }

  changeCompletionStatus = (index) => {
    console.log(index);
    let todosCopy = this.state.todos.slice();
    todosCopy[index].is_completed = !this.state.todos[index].is_completed;
    this.setState({todos:todosCopy},()=>console.log('state updated'))
    fetch(root_url+todosCopy[index].UUID,{
      headers: headers,
      method:'put',
      body:JSON.stringify({'UUID':todosCopy[index].UUID, 'content':todosCopy[index].content, 'is_completed':todosCopy[index].is_completed})
    })
    return false;
  }

  onClick  = (text) => {
    let todosCopy = this.state.todos.slice();
    let uuid = Math.random().toString(36).substring(2);
    todosCopy.unshift({'content':text,'UUID':uuid});
    fetch(root_url,{
      headers: headers,
      method:'post',
      body:JSON.stringify({'content':text, 'UUID':uuid, 'position':todosCopy.length})
    })
    this.setState({todos: todosCopy});
    document.getElementById("primaryInput").value="";
  }

  deleteTodo = i =>{
    let todosCopy = this.state.todos.slice();
    let dataToDelete = todosCopy[i]
    fetch(root_url+dataToDelete.UUID,{
      headers: headers,
      method:'delete',
      //body:JSON.stringify({'UUID':dataToDelete.UUID})
    })
    todosCopy.splice(i,1);
    this.setState({todos:todosCopy})
  }
  
  updateToDo = (index, content) => {
    const old=this.state.todos[index];
    console.log(old.UUID);
    const latest = content;
    fetch(root_url+old.UUID,{
      headers: headers,
      method:'put',
      body:JSON.stringify({'UUID':old.UUID, 'content':latest})
    })
    let todosCopy = this.state.todos.slice();
    todosCopy[index].content = content;
    this.setState({todos:todosCopy})
  }

  reposition = () =>{
    let textFields = this.state.todos.map((e,i)=>e.UUID);
      fetch(root_url+'reposition/',{
      headers: headers,
      method:'post',
      body:JSON.stringify({'list':textFields})
    })
    
  }

  updateDate = (index, value) => {
    console.log(index, value);
    let todosCopy = this.state.todos;
    todosCopy[index].dead_line = value;
    this.setState({todos:todosCopy})
    fetch(root_url+todosCopy[index].UUID,{
      headers: headers,
      method:'put',
      body:JSON.stringify({'UUID':todosCopy[index].UUID, 'content':todosCopy[index].content, 'dead_line':value})
    })

  }
  //   This code handles all drag and drop features
  // 
  // 
  handleDragStart = (e) => {
    // Target (this) element is the source node.
    //this.state.dragSrcEl = this;
    this.setState({dragSrcEl:e.target})
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.outerHTML);
    e.target.classList.add('dragElem');
  }

  handleDragOver = (e)=> {
    if (e.preventDefault) {
      e.preventDefault(); // Necessary. Allows us to drop.
    }
    e.target.classList.add('over');
    e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
    return false;
  }

  handleDragEnter = (e) => {
    // this / e.target is the current hover target.
  }

  handleDragLeave=(e) =>{
    e.target.classList.remove('over');  // this / e.target is previous target element.
  }

  handleDrop=(e) =>{
    // this/e.target is current target element.
    let old_index = Number(this.state.dragSrcEl.id);
    let old_content = this.state.todos[old_index];
    let new_index = Number(e.target.id);

    // Don't do anything if dropping the same column we're dragging.
    if (this.state.dragSrcEl !== this) {
      let todosCopy = this.state.todos.slice();

      todosCopy.splice(old_index,1)
      todosCopy.splice(new_index,0,old_content)
      console.log(old_content)
      this.setState({todos:todosCopy},()=>this.reposition());
      
    }
    e.target.classList.remove('over');
  }

  handleDragEnd = (e) =>{
    e.target.classList.remove('over');
    e.preventDefault();
  }


  // 
  // 
  // 
  // 

  render(){
    let bulletTodos = this.state.todos.map((e,i)=>{
          let properties = {};
          properties.todo=e.content;
          properties.is_completed = e.is_completed;
          properties.deadline = e.dead_line
          properties.key=i;
          properties.index=i;
          properties.update=(i,text)=>{this.updateToDo(i,text)};
          properties.delete=()=>{this.deleteTodo(i)};
          properties.changeCompletionStatus = () => {this.changeCompletionStatus(i)};
          properties.updateDate = (i, date) => {this.updateDate(i,date)}
          
          let dragprops={}
          dragprops.onDragStart=(event) => this.handleDragStart(event);
          dragprops.onDragEnd=(event)=>this.handleDragEnd(event);
          dragprops.onDragEnter=(event)=>this.handleDragEnter(event);
          dragprops.onDragOver=(event)=>this.handleDragOver(event);
          dragprops.onDragLeave=(event)=>this.handleDragLeave(event);
          dragprops.onDrop=(event)=>this.handleDrop(event);
          dragprops.id=i.toString();


          
          properties.dragprops=dragprops;

         return <SingleTodo {...properties}/>
    })
    return (
      
      <div>
        <InputForm text={this.state.currentToDo} onInputChange={(event)=> this.onInputChange(event)}/>
        <ul id="sortable" >{bulletTodos}</ul>
      </div>
    )
  }
}

export default App;
