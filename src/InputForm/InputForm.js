import React, { Component } from "react";

class InputForm extends Component{
render(){
        return (

            <input id="primaryInput" value={this.props.text}
                placeholder="What is your next Goal"
                type="input"
                
                onKeyUp={
                  event => {
                      this.props.onInputChange(event);
                    }
                  
                  }
              />
        )
    }
}

export default InputForm;